<p align="center"><img src="https://cdn.worldvectorlogo.com/logos/jwtio-json-web-token.svg"></p>


## Introduction

It library for using Laravel Passport (OAuth2) server and API authentication package that is simple and enjoyable to use.

## Instalation
+ add to composer.json 
	```
	"repositories": [
    	{
        	"type": "vcs",
        	"url": "https://gitlab.com/mnemchenko/eldo-auth-pachage"
    	}
	]
	```

+ `composer required eldorado/auth-to-service`

+ add to config/app.php 
	```
		Eldorado\Auth\Providers\AuthToServiceProvider::class,
	```

+ ```php artisan vendor:publish --tag=eldorado-auth-config```

+ (!!! If DOCTRINE installed) ```php artisan doctrine:generate:entity```

+ check in .env file `AUTH_SERVICE_URL`, `AUTH_SERVICE_CLIENT_ID`, `AUTH_SERVICE_CLIENT_SECRET`, `AUTH_SERVICE_MASTER_TOKEN`

+ change `auth.php` guards.api.driver from `token ` to `eldoradoJwt`

+ add to `Kernel.php` $routeMiddleware new item 
	```
		'eldoradoJwt' => \Eldorado\Auth\Middleware\JwtAuthenticate::class,
	```

+ add to route, controller, etc. middleware `eldoradoJwt` as advised by document https://laravel.com/docs/7.x/middleware

+ not nessary. change `Handler.php` to `customApiResponse->swith->HTTP_UNAUTHORIZED` to 
	```
		$response['message'] = $responseObj->original->getMessage();
	```
