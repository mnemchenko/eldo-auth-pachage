<?php

return [
    'auth' => [
        'clientId' => env('AUTH_SERVICE_CLIENT_ID'),
        'clientSecret' => env('AUTH_SERVICE_CLIENT_SECRET'),
        'masterToken' => env('AUTH_SERVICE_MASTER_TOKEN'),
        'url' => env('AUTH_SERVICE_URL'),
    ],
    'throttle' => [
        'maxAttemps' => env('MAX_LOGIN_ATTEMPS', 3),
        'decayMinutes' => env('DECAY_MINUTES', 10),
    ],
];