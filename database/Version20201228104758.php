<?php

namespace database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;
use LaravelDoctrine\Migrations\Schema\Table;
use LaravelDoctrine\Migrations\Schema\Builder;

class Version20201228104758 extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(Schema $schema): void
    {
        (new Builder($schema))->create('clients', function (Table $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->string('name');

            $table->dateTime('created_at')->setDefault('CURRENT_TIMESTAMP');
            $table->dateTime('updated_at')->setDefault('CURRENT_TIMESTAMP');

            $table->index('client_id');
            $table->index('name');
            $table->unique('client_id');
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(Schema $schema): void
    {
    }
}
