<?php

namespace Eldorado\Auth\Controllers;

use Illuminate\Http\Request;
use Eldorado\Auth\Requests\AuthLoginRequest;
use Eldorado\Auth\Requests\AuthRefreshRequest;
use Eldorado\Auth\Requests\AuthRegisterRequest;
use Eldorado\Auth\Services\AuthServiceInterface;

/**
 * Class for create new terminal-item in storage
 */
class AuthController extends \Illuminate\Routing\Controller
{	
	private $authService;

    /**
     * AuthController constructor.
     * @param AuthServiceInterface $authService
     */
    public function __construct(AuthServiceInterface $authService)
	{
		$this->authService = $authService;
		$this->middleware('throttle:10,1')->only('getCredentials');
	}

    /**
     * @param AuthLoginRequest $request
     * @return array|mixed|null
     */
    public function getCredentialsPassword(AuthLoginRequest $request): ?array
    {
        $result = $this->authService->getCredentialsPassword($request->all(), $request);
        return $result['data'] ?? $result;
    }

    /**
     * @param AuthLoginRequest $request
     * @return array|mixed|null
     */
    public function getCredentials(AuthLoginRequest $request): ?array
    {
		$result = $this->authService->getCredentials($request->all(), $request);
		return $result['data'] ?? $result;
	}

    /**
     * @param AuthRefreshRequest $request
     * @return array|mixed|null
     */
    public function refreshToken(AuthRefreshRequest $request): ?array
    {
		$result = $this->authService->refreshToken($request->validated());
		return $result['data'] ?? $result;
	}

    /**
     * @param Request $request
     * @return mixed
     */
    public function callback(Request $request)
	{
		$result = $this->authService->callback($request);
		return $result['data'] ?? $result;
	}
}