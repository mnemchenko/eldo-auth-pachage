<?php

namespace Eldorado\Auth;

use Throwable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Auth\AuthenticationException;

class EldoradoJwtGuard implements Guard
{
	use \Illuminate\Auth\GuardHelpers;

	/**
	 * The request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	protected $request;
	
	protected $inputKey = 'token';
	
	protected $decodeJwt;

	/**
	 * The name of the query string item from the request containing the API token.
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * The name of the token "column" in persistent storage.
	 *
	 * @var string
	 */
	protected $config;

	/**
	 * Create a new authentication guard.
	 *
	 * @param  $container
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $name
	 * @param  string  $config
	 * @param  bool  $hash
	 * @return void
	 */
	public function __construct(
	    $container,
	    Request $request,
	    $name,
	    array $config)
	{
	    $this->request = $request;
	    $this->name = $name;
	    $this->config = $config;
	}

	/**
	 * Get the currently authenticated user.
	 *
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function user()
	{
	    $user = null;

	    $token = $this->getTokenForRequest();
	    if (!empty($token) && $this->validate([$this->inputKey => $token])) {
	    	$user = new Token($this->decodeJwt);
	    }

	    return $this->user = $user;
	}

	/**
	 * Get the token for the current request.
	 *
	 * @return string
	 */
	public function getTokenForRequest()
	{
	    $token = $this->request->query($this->inputKey);

	    if (empty($token)) {
	        $token = $this->request->input($this->inputKey);
	    }

	    if (empty($token)) {
	        $token = $this->request->bearerToken();
	    }

	    if (empty($token)) {
	        $token = $this->request->getPassword();
	    }

	    return $token;
	}

	/**
	 * Validate a user's credentials.
	 *
	 * @param  array  $credentials
	 * @return bool
	 */
	public function validate(array $credentials = [])
	{
		try {
			$token = JWT::decode($credentials[$this->inputKey], $this->getPublicJwt(), ['RS256']);
			$this->decodeJwt = json_decode(json_encode($token), true);
			
			return true;	
		} catch (Throwable $e) {
			throw new AuthenticationException('Unauthenticated. '.$e->getMessage(), [$this]);
		}
	}

	/**
	 * Set the current request instance.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return $this
	 */
	public function setRequest(Request $request)
	{
	    $this->request = $request;

	    return $this;
	}

	private function getPublicJwt()
	{
		$guzzle = new Client;
		$response = $guzzle->get(config('eldoradoAuth.auth.url').'/api/public');
		$data = json_decode((string) $response->getBody(), true);
        return $data['data']['public'] ?? null;
	}
}