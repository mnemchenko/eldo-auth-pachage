<?php

namespace Eldorado\Auth\Exceptions;

use Exception;

/**
 * InvalidConfigException represents an exception caused by incorrect object configuration.
 *
 * @author hack0013 <hack0013@gmail.com>
 */
class ConflictException extends Exception
{
    public function __construct(
        $message = 'Conflict',
        $code = 409,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
    
    public function getStatusCode()
    {
        return $this->code;
    }
}
