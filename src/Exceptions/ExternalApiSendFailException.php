<?php

namespace Eldorado\Auth\Exceptions;

use Exception;

/**
 * ExternalApiSendFailException represents an exception caused by status of sent request to API.
 *
 * @author hack0013 <hack0013@gmail.com>
 */
class ExternalApiSendFailException extends Exception
{
    public function __construct(
        $message = 'External error',
        $code = 503,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
    public function getStatusCode()
    {
        return $this->code;
    }
}
