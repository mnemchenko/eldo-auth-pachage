<?php

namespace Eldorado\Auth\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class JwtAuthenticate extends Middleware
{
	protected function authenticate($request, array $guards)
	{
	    if (empty($guards)) {
	        $guards = [null];
	    }

	    foreach ($guards as $guard) {
	        if ($this->auth->guard($guard)->check()) {
	            $user = $this->auth->shouldUse($guard);
	        	$this->checkTokenAccess($request, $user);
	        	return $user;
	        }
	    }

	    $this->unauthenticated($request);
	}

	protected function checkTokenAccess($request, $user)
	{
		// check scopes and other data hached in token
		// in fail call $this->unauthenticated($request, $guards);
		
		
	}

	protected function unauthenticated($request, array $guards = [])
	{
	    parent::unauthenticated($request, $guards);
	}
}