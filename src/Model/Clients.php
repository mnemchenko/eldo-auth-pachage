<?php

namespace Eldorado\Auth\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;

    protected $table = 'clients';

    protected $fillable = ['name', 'client_id'];

    public function getClientId()
    {
        return $this->client_id;
    }
}