<?php

namespace Eldorado\Auth\Providers;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Eldorado\Auth\Model\Clients;
use Eldorado\Auth\Repositories\ClientRepository;
use Eldorado\Auth\Repositories\Interfaces\ClientRepositoryInterface;
use Route;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Eldorado\Auth\RouteRegistrar;
use Eldorado\Auth\Services\AuthServiceInterface;
use Eldorado\Auth\Services\AuthService;
use Eldorado\Auth\EldoradoJwtGuard;
use Eldorado\Auth\Repositories\EloquentClientRepository;

class AuthToServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $migrationDoctrine = [
            __DIR__.'/../../database/Version20201228104758.php' => database_path('migrations/Version20201228104758.php'),
        ];
        $migrationEloquent = [
            __DIR__.'/../../database/2020_01_01_111111_clients.php' => database_path('migrations/2020_01_01_111111_clients.php')
        ];
        $migration = class_exists('Doctrine\Migrations\AbstractMigration') ? $migrationDoctrine : $migrationEloquent;
        $config = [
            __DIR__.'/../../config/eldoradoAuth.php' => config_path('eldoradoAuth.php'),
        ];
        $this->publishes(array_merge($config, $migration), 'eldorado-auth-config');


        if (class_exists('Doctrine\ORM\EntityRepository')) {

            $this->app->when(ClientRepository::class)
                ->needs(EntityRepository::class)
                ->give(function () {
                    return $this->buildEntityRepository('app\Entity\Clients');
                });
            $this->app->singleton(ClientRepositoryInterface::class, ClientRepository::class);

        } else {
            $this->app->singleton(ClientRepositoryInterface::class, function ($app) {
                $model = new Clients();
                return new EloquentClientRepository($model);
            });
        }

        $this->app->singleton(AuthServiceInterface::class, function ($app) {
            $guzzle = new Client;
            $repository = $app->make(ClientRepositoryInterface::class);
            return new AuthService($guzzle, $repository);
        });

        Auth::extend('eldoradoJwt', function ($app, $name, array $config) {
            return new EldoradoJwtGuard($app, $app->request, $name, $config);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        self::routes();
    }

    private function buildEntityRepository(string $entity)
    {
        return (new EntityRepository(
            $this->app->make(EntityManagerInterface::class),
            new ClassMetadata($entity)
        ));
    }

    /**
     * Binds routes into the controller.
     *
     * @param  callable|null  $callback
     * @param  array  $options
     * @return void
     */
    public static function routes($callback = null, array $options = [])
    {
        $callback = $callback ?: function ($router) {
            $router->all();
        };

        $defaultOptions = [
            'prefix' => 'api/auth',
            'namespace' => '\Eldorado\Auth\Controllers',
            'middleware' => 'api',
        ];

        $options = array_merge($defaultOptions, $options);

        Route::group($options, function ($router) use ($callback) {
            $callback(new RouteRegistrar($router));
        });
    }
}
