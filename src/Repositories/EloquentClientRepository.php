<?php

namespace Eldorado\Auth\Repositories;

use Eldorado\Auth\Model\Clients;
use Eldorado\Auth\Repositories\Interfaces\ClientRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EloquentClientRepository implements ClientRepositoryInterface
{
    protected $modelClass = Clients::class;

    public function create($client): void
    {
        $client->save();
    }

    public function update($client): void
    {
        $client->save();
    }

    public function remove($client): void
    {
        $client->delete();
    }

    public function findClientByName(string $name)
    {
        return $this->modelClass::firstWhere('name', $name);
    }
}
