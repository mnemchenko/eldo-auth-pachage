<?php

namespace Eldorado\Auth\Repositories\Interfaces;

Interface ClientRepositoryInterface
{
	public function create($client): void;
    public function update($client): void;
    public function remove($client): void;
    public function findClientByName(string $name);
}
