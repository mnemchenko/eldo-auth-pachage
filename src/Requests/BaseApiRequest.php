<?php

namespace Eldorado\Auth\Requests;

use Eldorado\Auth\Exceptions\ConflictException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseApiRequest extends FormRequest
{  
    protected function prepareForValidation()
    {
        /* validate input json */
        if ($this->isJson()) {
            $this->validateJson();
        } else {
            throw new ConflictException("Content-Type must be application/json in request header");
        }
    }
    
    protected function validateJson()
    {
        $content = $this->getContent();
        $array = json_decode($content, true);
        $jsonErrors = [
            JSON_ERROR_DEPTH,
            JSON_ERROR_STATE_MISMATCH,
            JSON_ERROR_CTRL_CHAR,
            JSON_ERROR_SYNTAX,
            JSON_ERROR_UTF8,
            JSON_ERROR_RECURSION,
            JSON_ERROR_INF_OR_NAN,
            JSON_ERROR_UNSUPPORTED_TYPE,
            JSON_ERROR_INVALID_PROPERTY_NAME,
            JSON_ERROR_UTF16
        ];
        if ('' !== $content && in_array(json_last_error(), $jsonErrors)) {
            $errorMsg = json_last_error_msg();
            throw new ConflictException('JsonError '. $errorMsg);
        }

        return true;
    }

    /**
     * Handle a failed validation attempt without redirect
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $exception = ValidationException::withMessages($validator->errors()->toArray());
        throw $exception;
    }
}
