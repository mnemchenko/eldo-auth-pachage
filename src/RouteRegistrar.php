<?php

namespace Eldorado\Auth;

use Illuminate\Contracts\Routing\Registrar as Router;

class RouteRegistrar
{
    /**
     * The router implementation.
     *
     * @var \Illuminate\Contracts\Routing\Registrar
     */
    protected $router;

    /**
     * Create a new route registrar instance.
     *
     * @param  \Illuminate\Contracts\Routing\Registrar  $router
     * @return void
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Register routes for transient tokens, clients, and personal access tokens.
     *
     * @return void
     */
    public function all()
    {
        $this->router->group(['middleware' => []], function ($router) {
            $router->post('get-credentials-password', [
                'uses' => 'AuthController@getCredentialsPassword',
                'as' => 'eldorado.auth.getCredentialsPassword',
            ]);
            $router->post('get-credentials', [
                'uses' => 'AuthController@getCredentials',
                'as' => 'eldorado.auth.getCredentials',
            ]);
            $router->post('refresh-token', [
                'uses' => 'AuthController@refreshToken',
                'as' => 'eldorado.auth.refreshToken',
            ]);
            $router->get('callback', [
                'uses' => 'AuthController@callback',
                'as' => 'eldorado.auth.callback',
            ]);
        });
    }
}
