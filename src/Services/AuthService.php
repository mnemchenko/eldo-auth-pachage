<?php

namespace Eldorado\Auth\Services;

use Eldorado\Auth\Exceptions\ExternalApiSendFailException;
use Eldorado\Auth\Model\Clients;
use Eldorado\Auth\Repositories\Interfaces\ClientRepositoryInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use GuzzleHttp\ClientInterface;
use Exception;
use Illuminate\Validation\ValidationException;

class AuthService implements AuthServiceInterface
{
    use ThrottlesLogins;

    protected $maxAttempts;
    protected $decayMinutes;

    protected $clientRepository;
    protected $guzzleClient;
    private $client;

    /**
     * AuthService constructor.
     * @param ClientInterface $guzzleClient
     * @param ClientRepositoryInterface $clientRepository
     */
    public function __construct(ClientInterface $guzzleClient, ClientRepositoryInterface $clientRepository)
    {
        $this->guzzleClient = $guzzleClient;
        $this->clientRepository = $clientRepository;

        $this->maxAttempts = config('eldoradoAuth.throttle.maxAttemps') ?? 3;
        $this->decayMinutes = config('eldoradoAuth.throttle.decayMinutes') ?? 1;
    }

    protected function findClient($request)
    {
        $name = $request->name;
        $client = $this->clientRepository->findClientByName($name);
        if (!$client) {
            $this->incrementLoginAttempts($request);
            throw new AuthorizationException();
        }
        return $client;
    }

    /**
     * @param array $data
     * @param $request
     * @return array|null
     */
    public function getCredentialsPassword(array $data, $request): ?array
    {
        /* block request if limit attemps exceeded */
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        /* +1 to attemps if user not found */
        $client = $this->findClient($request);
        try {
            $response = $this->guzzleClient->post(config('eldoradoAuth.auth.url').'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $client->getClientId(),
                    'client_secret' => $data['password'] ?? null,
                    'scope' => '*',
                ],
            ]);

            $body = json_decode($response->getBody()->getContents(), true);
            $this->clearLoginAttempts($request);

            return $body['data'] ?? [];
        } catch (ClientException $e) {
            /* +1 to attemps if request fail */
            $this->incrementLoginAttempts($request);
            $this->prepareClientException($e);
        } catch (ServerException $e) {
            $this->prepareServerException($e);
        } catch (\Throwable $e) {
            throw new ExternalApiSendFailException('Fail when send auth request');
        }
    }

    /**
     * @param array $data
     * @param $request
     * @return array|null
     */
    public function getCredentials(array $data, $request): ?array
    {
        die('In progress. Not need now');

        try {
            $response = $this->guzzleClient->get(config('eldoradoAuth.auth.url').'/oauth/authorize?'.
                http_build_query([
                    'client_id' => $this->client,
                    'redirect_uri' => $request->root().'/api/auth/callback',// ,ссылка на ресурс, который делает запрос get-сredentials. там должен быть путь callback для принятия запроса и последующей передачи секрета и кода авторизации
                    'response_type' => 'code',
                    'scope' => 'service-request',
                ]),
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Content-Type'	=> 'application/json',
                        'Authorization' => config('eldoradoAuth.auth.masterToken'),
                    ],
                ]
            );
            $body = json_decode($response->getBody()->getContents(), true);
            return $body['data'] ?? [];
        } catch (ClientException $e) {
            $this->prepareClientException($e);
        } catch (ServerException $e) {
            $this->prepareServerException($e);
        }
    }

    /**
     * @param array $data
     * @return array|null
     */
    public function refreshToken(array $data): ?array
    {
        die('In progress. Not need now');
        $response = $this->guzzleClient->post(config('eldoradoAuth.auth.url').'/oauth/token', [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $data['refreshToken'],
                'client_id' => config('eldoradoAuth.auth.clientId'),
                'client_secret' => config('eldoradoAuth.auth.clientSecret'),
                'scope' => 'service-request',
            ],
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function callback(Request $request)
    {
        die('In progress. Not need now');
        $data = $request->all();

        $response = $this->guzzleClient->post(config('eldoradoAuth.auth.url').'/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => config('eldoradoAuth.auth.clientId'),
                'client_secret' => config('eldoradoAuth.auth.clientSecret'),
                'redirect_uri' => $request->root().'/api/auth/callback',
                'code' => $data['code'] // Get code from the callback
            ]
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    /**
     * @return string
     */
    protected function username()
    {
        return 'name';
    }

    protected function prepareClientException(ClientException $e)
    {
        $response = json_decode($e->getResponse()->getBody()->getContents(), true);
        $message = $response['message'] ?? '';
        $errors = !empty($response['errors']) ? json_encode($response['errors']) : '';

        throw new ExternalApiSendFailException(
            'External service error. '.$message.$errors
        );
    }

    protected function prepareServerException(ServerException $e)
    {
        $response = json_decode($e->getResponse()->getBody()->getContents(), true);
        throw new ExternalApiSendFailException('External service error. '.$response['message'] ?? '');
    }
}