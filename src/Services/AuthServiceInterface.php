<?php

namespace Eldorado\Auth\Services;

use Illuminate\Http\Request;

interface AuthServiceInterface
{
	public function refreshToken(array $input): ?array;
	public function getCredentials(array $input, $request): ?array;
	public function getCredentialsPassword(array $input, $request): ?array;
	public function callback(Request $request);
}